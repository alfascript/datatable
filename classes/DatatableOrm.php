<?php defined('SYSPATH') or die();

/**
 * Class DatatableOrm
 */
class DatatableOrm
{
    /**
     * For data
     * @var ORM 
     */
    protected $_orm;
    
    /**
     * For count total
     * @var ORM 
     */
    protected $_ormTotal;
    
    /**
     * For count filtered
     * @var ORM 
     */
    protected $_ormFiltered;
    
    /**
     * POST from JQuery Datatable plugin
     * @var array
     */
    protected $_request;
    
    /**
     * 
     * @var boolean
     */
    protected $_isFiltering = false;

    /**
     * @param string $nameOrm
     * @return DatatableOrm
     */
    public static function factory($nameOrm, $columns, $request)
    {
        return new self($nameOrm, $columns, $request);
    }

    /**
     * @param $nameOrm
     */
    public function __construct($nameOrm, $columns, $request)
    {
        $this->_orm = ORM::factory($nameOrm);
        $this->_ormTotal = clone $this->_orm;
        $this->_ormFiltered = clone $this->_orm;
        
        $this->_columns = $columns;
        $this->_request = $request;
        
        // Global Search
        if ( isset($this->_request['search']) && ! empty($this->_request['search']['value']) )
        {
            $isSearchable = false;
            foreach($this->_columns as $column)
            {
                if ( isset($column['searchable']) && $column['searchable'] )
                {
                    $isSearchable = true;
                    break;
                }
            }
            
            if ( $isSearchable )
            {
                $this->filter_open();
                $this->where_open();
                foreach($this->_columns as $column)
                {
                    if ( isset($column['searchable']) && $column['searchable'] )
                    {
                        $alias = (isset($column['relationship']) ? $column['relationship'] : $this->_orm->object_name()).'.'.( ! is_string($column['data']) && is_callable($column['data']) ? $column['alias'] : $column['data'] );
                        $this->or_where($alias, 'LIKE', self::quote($this->_request['search']['value'], '%'));
                    }
                }
                $this->where_close();
                $this->filter_close();
            }
        }

        // Inner join with related rows
        foreach($this->_columns as $column)
        {
            if ( isset($column['relationship']) )
            {
                $this->with($column['relationship']);
            }
        }
    }
    
    /**
     * 
     */
    public function filter_open()
    {
        $this->_isFiltering = true;
    }

    /**
     * 
     */
    public function filter_close()
    {
        $this->_isFiltering = false;
    }
    
    /**
     * Retranslate a call to $this->_orm
     * @param string $name
     * @param array $arguments
     * @return mixed usually $this
     */
    public function __call($name, $arguments)
    {
        if ( 'group_by' == $name )
        {
            throw new Exception("Group_by is not support, use subquery with group_by instead");
        }
        
        $result = call_user_func_array(array($this->_orm, $name), $arguments);
        if ( $result !== $this->_orm )
        {
            throw new Exception("Called method '$name' which should not be called");
        }
        
        if ( 'order_by' != $name )
        {
            call_user_func_array(array($this->_ormFiltered, $name), $arguments);
            
            if ( ! $this->_isFiltering )
            {
                call_user_func_array(array($this->_ormTotal, $name), $arguments);
            }
        }
        
        return $this;
    }
    
    /**
     * Send the json encoded result for DataTables.js
     *
     * @param $request
     * @return string json output
     */
    public function getData()
    {
        // Limit
        if ( isset($this->_request['start']) && $this->_request['length'] != -1 )
        {
            $this->_orm->offset($this->_request['start']);
            $this->_orm->limit($this->_request['length']);
        }
        
        // Order by
        if ( isset($this->_request['order']) && count($this->_request['order']) > 0 )
        {
            foreach($this->_request['order'] as $order)
            {
                $columnIdx = (int)$order['column'];
                $column = $this->_columns[$columnIdx];
                if ( isset($column['orderable']) && $column['orderable'] )
                {
                    $alias = Arr::get($column, 'alias', $column['data']);
                    $this->_orm->order_by($alias, $order['dir']);
                }
            }
        }
        
        // Get data
        try
        {
            $rows = $this->_orm->find_all();
        }
        catch(DatabaseException $e)
        {
            throw new Exception('Ошибка в запросте DatatableOrm код:'.$e->getCode()." \nсообщение:".$e->getMessage());
        }
        
        $data = array();
        foreach($rows as $row)
        {
            $rowDatatable = array();
            foreach($this->_columns as $column)
            {
                $alias = Arr::get($column, 'alias', $column['data']);
                if ( isset($column['relationship']) )
                {
                    $rowDatatable[$alias] = isset($row->{$column['relationship']}) && isset($row->{$column['relationship']}->{$column['data']}) ? $row->{$column['relationship']}->{$column['data']} : null;
                }
                elseif ( ! is_string($column['data']) && is_callable($column['data']) )
                {
                    $rowDatatable[$alias] = $column['data']($row);
                }
                else
                {
                    $rowDatatable[$alias] = $row->{$column['data']};
                }
            }
            $data[] = $rowDatatable;
        }
        
        $recordsFiltered = $this->_ormFiltered->count_all();
        $recordsTotal = $this->_ormTotal->count_all();

        $jsonResponse = array(
            'draw' => $this->_request['draw'],
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data,
        );
        return $jsonResponse;
    }

    /**
     * Quote and Protect string for sql query
     *
     * @param string $str String to quote
     * @param string $encapsuler Encapsulate the string without being protected (for Like % eg)
     * @return string
     */
    static function quote($str, $encapsuler = '')
    {
        if(empty($encapsuler) && (is_int($str) || ctype_digit($str)))
        {
            return $str;
        }
        return $encapsuler.addcslashes($str, '%_\'').$encapsuler;
    }

}
